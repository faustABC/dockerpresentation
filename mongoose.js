const mongoose = require('mongoose');

const connectionUrl = 'mongodb://localhost/dockerPresentation';

mongoose.connect(connectionUrl, { useNewUrlParser: true });
mongoose.Promise = global.Promise;

mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));
