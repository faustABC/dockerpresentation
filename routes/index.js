const router = require('express').Router();
const SquareModel = require('../models/square.model');

router.get('/', (req, res) => {
  res.send('Hello World!');
});

router.get('/hello', (req, res) => {
  res.send('Hello Hello Hello Hello Hello Hello Hello !');
});

router.put('/square', async (req, res) => {
  const square = await SquareModel.create({ size: 100 });
  res.send(square);
});

module.exports = router;
