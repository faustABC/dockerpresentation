const { Schema, model } = require('mongoose');

const SquareSchema = new Schema({
  size: Schema.Types.Number,
});

module.exports = model('Square', SquareSchema);
